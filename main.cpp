#include <iostream>
#include <chrono>
#include <string>
#include <thread>
#include "clipp.h"
#include "ryzenadj.h"

static const int DEFAULT_DELAY = 500;
static const int DEFAULT_STAPM = 15000;
static const int DEFAULT_SLOW  = 17000;
static const int DEFAULT_FAST  = 20000; 

static void fixSettings(int delay, int stapmLimit, 
                        int pptSlowLimit, int pptFastLimit)
{
  const auto cpu = init_ryzenadj(); 

  while (true) {
    set_stapm_limit(cpu, stapmLimit);
    set_slow_limit(cpu, pptSlowLimit);
    set_fast_limit(cpu, pptFastLimit);

    std::this_thread::sleep_for(std::chrono::milliseconds(delay));
  }
}

int main(int argc, char *argv[])
{
  int delay = DEFAULT_DELAY, stapmLimit = DEFAULT_STAPM, 
      pptSlowLimit = DEFAULT_SLOW, pptFastLimit = DEFAULT_FAST;

  const auto cli = (
    clipp::option("-d", "--delay") & 
      clipp::value("refresh-delay")
        .doc("Refresh delay in milliseconds. Defaults to " + std::to_string(DEFAULT_DELAY))
        >> delay,

    clipp::option("-p", "--stapm-limit") & 
      clipp::value("stapm-limit")
        .doc("STA(P)M limit in milliwatts. Defaults to " + std::to_string(DEFAULT_STAPM))
        >> stapmLimit,

    clipp::option("-s", "--slow-limit") & 
      clipp::value("slow-limit")
        .doc("'Slow' limit in milliwatts. Defaults to " + std::to_string(DEFAULT_SLOW))
        >> pptSlowLimit,

    clipp::option("-f", "--fast-limit") & 
      clipp::value("fast-limit")
        .doc("'Fast' limit in milliwatts. Defaults to " + std::to_string(DEFAULT_FAST))
        >> pptFastLimit
  );

  if (clipp::parse(argc, argv, cli)) {
    fixSettings(delay, stapmLimit, pptSlowLimit, pptFastLimit);
  } else {
    std::cout << "Usage:\n" << clipp::usage_lines(cli, argv[0]) << "\n\n"
              << "Options:\n" << clipp::documentation(cli) << '\n';
  }

  return 0;
}
