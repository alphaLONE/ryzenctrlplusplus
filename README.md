# RyzenCTRLplusplus

A Ryzen STAPM tool to fix the dang GA502DUs without draining the battery like RyzenController does (thanks Electron)

# How to run

Just download the contents of the /bin folder. Run RyzenAdjplusplus.exe with Administrative privileges, in the directory of the DLLs and the SYS file.

# I want to compile RC++

You can compile RyzenAdj++win.cpp with LLVM10 on Windows. Don't forget to include the libryzenadj.lib library.